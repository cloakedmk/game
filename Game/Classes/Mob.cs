﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GraveGame.Classes
{
    class Mob
    {

        private Animation Sprite;
        private Vector2 Position;
        public Vector2 Speed;

        public int Direction;

        public void Init(Animation mobTexture)
        {
            Sprite = mobTexture; 

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Position = new Vector2(0, 0);
            Sprite.Draw(spriteBatch, Position);
        }

        public void Update(GameTime gameTime)
        {
            if (Direction == 0)
            {
                //Sprite.Init();
            }
            //Sprite.Update(gameTime);
            Position.X += Speed.X;
            Position.Y += Speed.Y;
        }
    }
}
