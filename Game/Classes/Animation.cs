﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GraveGame.Classes
{
    class Animation
    {
        Rectangle sourceRect;
        Rectangle destRect;
        Texture2D spriteSheet;
        int spriteHeight;
        int spriteWidth;
        bool Active;
        bool Looping;
        int frameTime;
        int elapsedTime;
        int currentFrame;
        int totalFrames;
        public void Init(Texture2D sourceSheet, int Width, int frameLength, bool loop)
        {
            spriteSheet = sourceSheet;
            frameTime = frameLength;
            spriteWidth = Width;
            spriteHeight = spriteSheet.Height;
            Looping = loop;
            Active = true;
            currentFrame = 0;
            totalFrames = spriteSheet.Width / Width;
        }

        public void Update(GameTime gameTime)
        {
            if (!Active) { return; }

            elapsedTime = gameTime.ElapsedGameTime.Milliseconds;
            if (elapsedTime >= frameTime)
            {
                currentFrame++;
                if (currentFrame == totalFrames)
                {
                    currentFrame = 0;
                    if (!Looping)
                    {
                        Active = false;
                    }
                }
                elapsedTime = 0;
            }

            sourceRect = new Rectangle(currentFrame * spriteWidth, 0, spriteWidth, spriteHeight);
        }

        public void Draw(SpriteBatch spritebatch, Vector2 Position)
        {
            spritebatch.Draw(spriteSheet, Position, sourceRect, Color.White);
        }
    }
}
